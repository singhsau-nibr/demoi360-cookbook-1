name             'demoi360'
maintainer       'Novartis'
maintainer_email 'devops@nebula.na.novartis.net'
license          'All rights reserved'
description      'Installs/Configures demoi360'
#long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.4.25'

depends "mw-apache"
depends "ci"
depends "php"
depends "build-essential"
depends "nibr-oracle-client"
depends "anyapp"
