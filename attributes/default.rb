node.override['php']['version'] = "7.0.5"
default['proxy']['url'] = "usca-proxy01.na.novartis.net:2011"
default['demoi360']['dir1'] = "/var/www/demoi360"
default['demoi360']['user'] = "apache"
default['demoi360']['group'] = "apache"
default['apache']['dir']     = "/etc/httpd"
default['apache']['sslpath']    = "/etc/httpd/ssl"
override['file_server'] = 'http://nrusca-slp0002.nibr.novartis.net'
